(function() {
  'use strict';

  angular
    .module('yourRentalTest')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
