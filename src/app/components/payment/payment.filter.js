/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('yourRentalTest')
    .filter('creditNumberMask',function () {
      return function (creditNumber) {
        return "XXXX XXXX XXXX "+creditNumber.toString().substring(12,16);
      }
    })

})();
