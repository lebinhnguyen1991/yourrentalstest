(function () {
  'use strict';

  angular
    .module('yourRentalTest')
    .factory('paymentService', payment);

  /** @ngInject */
  function payment() {
    var paymentInfo = {
      default: null,
      paymentMethods: []
    };

    var paymentService = {
      loadPaymentMethods: function (moment) {
        return paymentInfo;
      },
      getCreditCardInstant: function () {
        return {
          id: moment().unix(),
          cardNumber: '',
          expiration: {
            month: '',
            year: ''
          },
          cvc: ''
        }
      },
      deleteCreditCard: function (index) {
        paymentInfo.paymentMethods.splice(index, 1);
      },
      addCreditCard: function (creditCardInstant) {
        paymentInfo.paymentMethods.push(creditCardInstant);
        // check if is the first credit card then make is as default
        if (paymentInfo.paymentMethods.length === 1) {
          paymentInfo.default = creditCardInstant.id;
        }
      }
    };

    return paymentService;
  }
})();
