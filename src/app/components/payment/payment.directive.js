(function () {
  'use strict';

  angular
    .module('yourRentalTest')
    .directive('payment', payment);

  /** @ngInject */
  function payment() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/payment/payment.html',
      scope: {
      },
      controller: PaymentController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function PaymentController(paymentService,toastr) {
      var vm = this;

      vm.paymentInfo = paymentService.loadPaymentMethods();

      // check a credit card is default method
      vm.isDefault = function (id) {
        return id === vm.paymentInfo.default;
      };

      // make a credit card as default method
      vm.makeDefault = function (id) {
        vm.paymentInfo.default = id;
      };

      // add payment
      vm.addPayment = function () {
        vm.isAdding = true;
        vm.newCredicard = paymentService.getCreditCardInstant();
      };

      // do add credit card
      vm.doAddPayment = function () {
        paymentService.addCreditCard(vm.newCredicard);

        vm.hideAddingForm();
      };

      // do cancel add form
      vm.doCancel = function () {
        vm.hideAddingForm();
      };

      // hide form
      vm.hideAddingForm = function () {
        vm.newCredicard = {};
        vm.isAdding = false;
      };

      // remove payment
      vm.removePayment = function (index) {
        var result=confirm('Do you want to delete this credit?');
        if(result){
          paymentService.deleteCreditCard(index);
          toastr.success("You have been delete a credit card","Success!");
        }
      };

      // alow only number 0-9 and backspace
      vm.numberKeyFilter=function (event) {
        console.log(event.keyCode);
        var keyCode = event.keyCode;
        if (keyCode!=9&&keyCode!=8&&(keyCode < 48 || keyCode > 57)) {
          event.preventDefault();
        }
      }
    }
  }

})();
