/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('yourRentalTest')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
