# README #

### What is this repository for? ###

* Your.Rentals Test
* Version 1.0

### How do I get set up? ###

I assume that you have already install nodejs, node-gyp, bower.

- Step 1: Clone project and cd to your project root path
- Step 2: Run command npm install then wait a couple minutes
- Step 3: Run command bower install
- Step 4: run command gulp serve and the website will be loaded in your brower shortly.

For case you can not set up and run the project, you can use the product version in zip file. You just extract zip file then run index.html file.

### Who do I talk to? ###

* Author: lebinhnguyen1991@gmail.com